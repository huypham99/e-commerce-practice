import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/common/product';
import { ActivatedRoute, Router } from '@angular/router';
import { DataExchangeService } from 'src/app/services/data-exchange.service';
import { CartService } from 'src/app/services/cart.service';
import { CartItem } from 'src/app/common/cart-item';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list-grid.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: Product[] = [];
  currentCategoryId: number = 1;
  previousCategoryId: number = 1;
  searchMode: boolean = false;
  message: string = '';

  // new properties for pagination
  thePageNumber: number = 1;
  thePageSize: number = 5;
  theTotalElements: number = 0;

  previousQ: string = null;

  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private router: Router,
              private dataService: DataExchangeService,
              private cartService: CartService) { }

  ngOnInit() {
    console.log(`>>> ProductListComponent.OnInit - Current route: ${this.router.url}`);
    this.route.paramMap.subscribe(() => {
      this.listProducts();
    });
    //this.dataService.currentMessage.subscribe(message => this.message = message);
    //this.newMessage(this.route.snapshot.paramMap.get('q'));
  }

  newMessage(message: string) {
    this.dataService.changeMessage(message);
  }

  listProducts() {
    
    this.searchMode = this.route.snapshot.paramMap.has('q');
    
    if (this.searchMode) {
      this.handleSearchProducts();
    }else {
      this.handleListProduct();
    }

  }

  handleSearchProducts() {

    const q: string = this.route.snapshot.paramMap.get('q');

    // if we have a different keyword than previous
    // then set thePageNumber to 1
    if (this.previousQ != q) {
      this.thePageNumber = 1;
    }
    
    this.previousQ = q;
    
    console.log(`keyword=${q}, thePageNumber=${this.thePageNumber}`);

    // now search for the products using keyword
    this.productService.searchProductsPaginate(this.thePageNumber - 1,
                                               this.thePageSize,
                                               q)
                                               .subscribe(this.processResult());
  }

  handleListProduct() {
    console.log('>>> ProductListComponent.listProducts()');

    // get current route
    const currentRoute = this.router.url;

    // if current route is /products then show all products
    // if (currentRoute === '/products') {
    //   this.productService.getAllProductList().subscribe(
    //     data => {
    //       this.products = data;
    //       this.currentCategoryId = 0;
    //     }
    //   )
    //   return;
    // }

    // check if "id" parameter is available
    const hasCategoryId: boolean = this.route.snapshot.paramMap.has('id');

    if (hasCategoryId) {
      // get the "id" param string. convert string to a number using the "+" symbol
      this.currentCategoryId = +this.route.snapshot.paramMap.get('id');
    }
    else {
      // not category id available ... default to category id 1
      this.currentCategoryId = 0;
    }

    //
    // Check if we have a different category than previous
    // Note: Angular will reuse a component if it is currently being viewed
    //

    // if we have different category than previous
    // then set thePageNumber back to 1
    if (this.previousCategoryId != this.currentCategoryId) {
      this.thePageNumber = 1;
    }

    this.previousCategoryId = this.currentCategoryId;

    console.log(`currentCategoryId=${this.currentCategoryId}, thePageNumber=${this.thePageNumber}`);

    // now get the products for the given category id
    this.productService.getProductListPaginate(this.thePageNumber - 1,
                                               this.thePageSize,
                                               this.currentCategoryId)
                                               .subscribe(this.processResult());
  }

  processResult() {
    return data => {
      this.products = data._embedded.products;
      this.thePageNumber = data.page.number + 1;
      this.thePageSize = data.page.size;
      this.theTotalElements = data.page.totalElements;
    };
  }

  updatePageSize(pageSize: number) {
    console.log(`ProductListComponent >>> updatePageSize ${pageSize}`);
    this.thePageSize = pageSize;
    this.thePageNumber = 1;
    this.listProducts();
  }

  addToCart(theProduct: Product) {
    console.log(`>>> ProductListComponent - Adding to cart: ${theProduct.name}, ${theProduct.unitPrice}`);
    
    // TODO ... do the real work
    const theCartItem = new CartItem(theProduct);

    this.cartService.addToCart(theCartItem);
  }

}