import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataExchangeService } from 'src/app/services/data-exchange.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchVal: string;
  message: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private dataService: DataExchangeService) { }

  ngOnInit(): void {
    console.log(`>>> SearchComponent.onInit: ${this.router.url}`);
    // this.searchVal = this.route.snapshot.paramMap.get('q');
    // this.dataService.currentMessage.subscribe(message => {
    //   this.message = message;
    //   this.searchVal = message;
    // });
  }

  doSearch(value: string) {
    console.log(`>>> doSeach(): value=${value}`);
    this.router.navigateByUrl(`/search/${value}`);
  }

}