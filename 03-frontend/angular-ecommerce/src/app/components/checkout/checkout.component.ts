import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { strictEqual } from 'assert';
import { Country } from 'src/app/common/country';
import { State } from 'src/app/common/state';
import { CartService } from 'src/app/services/cart.service';
import { Luv2ShopFormService } from 'src/app/services/luv2-shop-form.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  checkoutFormGroup: FormGroup;

  totalPrice: number = 0.00;
  totalQuantity: number = 0;

  expireMonths: number[] = [];
  expireYears: number[] = [];

  countries: Country[] = [];
  shippingAddressStates: State[] = [];
  billingAddressStates: State[] = [];

  constructor(private formBuilder: FormBuilder,
    private formService: Luv2ShopFormService,
    private cartService: CartService) { }

  ngOnInit(): void {

    this.checkoutFormGroup = this.formBuilder.group({
      customer: this.formBuilder.group({
        firstName: [''],
        lastName: [''],
        email: ['']
      }),
      shippingAddress: this.formBuilder.group({
        street: [''],
        city: [''],
        state: [''],
        country: [''],
        zipCode: ['']
      }),
      billingAddress: this.formBuilder.group({
        street: [''],
        city: [''],
        state: [''],
        country: [''],
        zipCode: ['']
      }),
      creditCard: this.formBuilder.group({
        cardType: [''],
        nameOnCard: [''],
        cardNumber: [''],
        securityCode: [''],
        expirationMonth: [''],
        expirationYear: ['']
      }),
    });

    let currentMonth = new Date().getMonth() + 1;
    let currentYear = new Date().getFullYear();
    console.log(`Current Month: ${currentMonth}`);
    console.log(`Current Year: ${currentYear}`);

    //populate credit card months
    this.formService.getCreditCardMonths(currentMonth).subscribe(
      data => {
        console.log(`Retrieved credit card months: ${JSON.stringify(data)}`);
        this.expireMonths = data;
      }
    );

    //populate credit card years
    this.formService.getCreditCardYears().subscribe(
      data => {
        console.log(`Retrieved credit card years: ${JSON.stringify(data)}`);
        this.expireYears = data;
      }
    );

    //populate countries
    this.formService.getCountries().subscribe(
      data => {
        console.log("Retrieved countries: " + JSON.stringify(data));
        this.countries = data;
      }
    );

    //populate total quantity
    this.cartService.totalQuantity.subscribe(
      data => this.totalQuantity = data
    );

    //populate total price
    this.cartService.totalPrice.subscribe(
      data => this.totalPrice = data
    )

    this.cartService.computeCartTotals();

  }

  handleChangeSelectedYear() {

    let creditCardGroup = this.checkoutFormGroup.get('creditCard');

    const currentYear: number = new Date().getFullYear();
    // parse to number
    const selectedYear = Number(creditCardGroup.value.expirationYear);
    let startMonth = 1;

    if (currentYear == selectedYear) {
      startMonth = new Date().getMonth() + 1;
    } else {
      startMonth = 1;
    }

    this.formService.getCreditCardMonths(startMonth).subscribe(
      data => {
        console.log(`Retrieved credit card months: ${JSON.stringify(data)}`);
        this.expireMonths = data;

        // select first month in list if user back to current year
        if (currentYear == selectedYear) {
          creditCardGroup.get('expirationMonth').setValue(data[0]);
        }
      }
    );

  }

  copyShippingAddressToBillAddress(event) {

    if (event.target.checked) {
      // copy shipping address value to billing address
      this.checkoutFormGroup.controls.billingAddress
        .setValue(this.checkoutFormGroup.controls.shippingAddress.value);

      this.getStatesForBillingCopyShipping();
      
    } else {
      // clear all input fields
      this.checkoutFormGroup.controls.billingAddress.reset();
    }

  }

  onSubmit() {
    console.log("Handling the submit button");
    console.log(this.checkoutFormGroup.get('customer').value);
    console.log(`The email address is ${this.checkoutFormGroup.get('customer').value.email}`);

    console.log(`The shipping address is ${this.checkoutFormGroup.get('shippingAddress').value.state.name}`);
    console.log(`The shipping address is ${this.checkoutFormGroup.get('shippingAddress').value.country.name}`);
  }

  getStates(formGroupName: string) {

    const formGroup = this.checkoutFormGroup.get(formGroupName);

    const countryCode = formGroup.value.country.code;
    const countryName = formGroup.value.country.name;

    console.log(`${formGroupName} country code: ${countryCode}`);
    console.log(`${formGroupName} country code: ${countryName}`);

    // populate states based on country code
    this.formService.getStates(countryCode).subscribe(
      data => {
        if (formGroupName === 'shippingAddress') {
          this.shippingAddressStates = data;
        } else {
          this.billingAddressStates = data;
        }
        
        // select first state item by default
        formGroup.get('state').setValue(data[0]);
      }
    );

  }

  getStatesForBillingCopyShipping() {

    const billingFormGroup = this.checkoutFormGroup.get('billingAddress');
    const shippingFormGroup = this.checkoutFormGroup.get('shippingAddress');

    const countryCode = billingFormGroup.value.country.code;

    // populate states based on country code
    this.formService.getStates(countryCode).subscribe(
      data => {
        this.billingAddressStates = data;

        let shippingStateName = shippingFormGroup.value.state.name;
        let i = 0;
        for (i = 0; i < data.length; i++) {
          if (data[i].name === shippingStateName) break;
        }
        
        // select first state item by default
        billingFormGroup.get('state').setValue(data[i]);
      }
    );

  }

}
