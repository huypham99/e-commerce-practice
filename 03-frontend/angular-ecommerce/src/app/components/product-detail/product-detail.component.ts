import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartItem } from 'src/app/common/cart-item';
import { Product } from 'src/app/common/product';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: Product = new Product();

  constructor(private productService: ProductService,
              private router: Router,
              private route: ActivatedRoute,
              private cartService: CartService
              ) { }

  ngOnInit(): void {
    console.log(`>>> ProductDetail.onInit: ${this.router.url}`);
    this.route.paramMap.subscribe(() => {
      this.showDetail();
    });
  }

  showDetail() {
    const productId = this.route.snapshot.paramMap.get("id");
    this.productService.getProduct(productId).subscribe(
      data => {
        this.product = data;
        console.log(`>>> ProductDetail.showDetail: ${this.product.name}`);
      }
    );
    
  }

  addToCart() {

    console.log(`>>> ProductDetailComponent - Adding to cart: ${this.product.name}, ${this.product.unitPrice}`);
    const theCartItem = new CartItem(this.product);
    this.cartService.addToCart(theCartItem);
    
  }

}
